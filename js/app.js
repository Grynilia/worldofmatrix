function myFunction(x) {
    x.classList.toggle("change");
}


const anchors = document.querySelectorAll('a[href*="#"]')

for (let anchor of anchors) {
    anchor.addEventListener('click', function (e) {
        e.preventDefault()

        const blockID = anchor.getAttribute('href').substr(1)

        document.getElementById(blockID).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        })
    })
}

var lang = document.querySelector('.lang');
lang.onchange = function(){
    if(lang.options[lang.options.selectedIndex].value == /ru/){
        window.location = 'https://worldofmatrix.info'
    }else
    window.location = 'https://worldofmatrix.info' + lang.options[lang.options.selectedIndex].value
    
    };


var langMobile = document.querySelector('.langMobile');
langMobile.onchange = function(){
    if(langMobile.options[langMobile.options.selectedIndex].value == /ru/){
        window.location = 'https://worldofmatrix.info'
    }else
        window.location = 'https://worldofmatrix.info' + langMobile.options[langMobile.options.selectedIndex].value
    
    };
